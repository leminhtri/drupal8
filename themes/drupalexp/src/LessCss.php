<?php

namespace Drupal\drupalexp;

use Drupal\drupalexp\DrupalExp;
use Drupal\drupalexp\Less\lessc;

class LessCss extends lessc {

  protected $input = '';
  protected $import = [];

  function __construct($fname = null) {
    parent::__construct($fname);
    $this->setImportDir(getcwd());
  }

  public function setVariable($key, $value) {
    $variable = [$key => $value];
    $this->setVariables($variable);
  }

  /**
   * 
   * @param type $string
   */
  public function add($string) {
    $this->input .= $string;
  }

  /**
   * import file
   * @param type $file
   */
  public function import($file) {
    if(is_array($file)){
      $this->import = array_merge($this->import, $file);
    }else{
      $this->import[] = $file;
    }
  }
  
  /**
   * 
   * @return type
   */
  protected function getLessStr(){
    $LessStr = '';
    foreach($this->import as $file){
      $LessStr .= "@import \"{$file}\";\n";
    }
    return $LessStr . "\n" . $this->input;
  }

  /**
   * 
   * @param type $string
   * @param type $out
   * @return boolean
   */
  public function compile($string = null, $out = null){
    if ($string === null){
      $string = $this->getLessStr();
      if($this->checkedCompile(null, $out) == false){
        return false;
      }
    }
    try {
      $theme = DrupalExp::get();
      if(is_dir($theme->getTheme()->getPath() . '/assets/images')){
        $this->copyImages($theme->getTheme()->getPath() . '/assets/images', 'public://drupalexp/' . $theme->getTheme()->getName() . '/images');
      }
      $data = parent::compile($string, null);
      file_unmanaged_save_data($data, $out, FILE_EXISTS_REPLACE);
      return $data;
    }
    catch (\Exception $e) {
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
    return FALSE;
  }

  // compile only if changed input has changed or output doesn't exist
  public function checkedCompile($in, $out) {
    if (!is_file($out)){
      return true;
    }else{
      $mtimeout = filemtime($out);
      foreach ($this->import as $file){
        if(is_file($file) && (filemtime($file) > $mtimeout)){
          return true;
        }
      }
    }
    return false;
  }

  public function copyImages($src, $dst) {
    $update = false;
    if (!is_dir($dst)) {
      file_prepare_directory($dst, FILE_CREATE_DIRECTORY);
      file_prepare_directory($dst, FILE_MODIFY_PERMISSIONS);
      $update = true;
    }
    if (filemtime($src) > filemtime($dst)) {
      $update = true;
    }
    if ($update) {
      if ($handle = @opendir($src)) {
        while (false !== ( $file = readdir($handle))) {
          if (( $file != '.' ) && ( $file != '..' )) {
            if (is_dir($src . '/' . $file)) {
              $this->copyImages($src . '/' . $file, $dst . '/' . $file);
            }
            else {
              file_unmanaged_copy($src . '/' . $file, $dst, FILE_EXISTS_REPLACE);
            }
          }
        }
        closedir($handle);
      }
      else {
        \Drupal::logger('file')->error('@dir can not be opened', array('@dir' => $src));
      }
    }
  }

  /**
   * 
   * @param type $page
   * @return LessCss
   */
  public static function get(&$page = null) {
    $LessCss = &drupal_static(__CLASS__ . __FUNCTION__);
    if (!isset($LessCss)) {
      $LessCss = new LessCss();
    }
    return $LessCss;
  }

}
