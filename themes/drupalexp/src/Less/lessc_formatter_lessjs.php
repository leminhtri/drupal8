<?php
namespace Drupal\drupalexp\Less;

use Drupal\drupalexp\Less\lessc_formatter_classic;

class lessc_formatter_lessjs extends lessc_formatter_classic {

  public $disableSingle = true;
  public $breakSelectors = true;
  public $assignSeparator = ": ";
  public $selectorSeparator = ",";

}