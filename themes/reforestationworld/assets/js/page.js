(function ($, Drupal, settings) {
  "use strict";

  Drupal.behaviors.dexp_proccess_page = {
    attach: function (context, settings) {
      $(document).ready(function () {
        $('.dexp-menu').find('.menu-item').each(function () {
          var $first_word = $(this).find('a').text().split(' ');
          $(this).find('a').html('<span>' + $first_word.shift() + '</span> ' + $first_word.join(' '));
        });
				$('.field-funding-sources').find('.process-bar').each(function(){
					var $value = $(this).data('value');
					if($value == 0.00){
						 $(this).closest('.field-funding-sources').css('display', 'none');
					}
				});
      });
      
      //Project detail popup
      
      $('.link-popup a').once('click').each(function(){
        $(this).on('click', function(e){
          e.preventDefault();
          var url = $(this).attr('href');
          Drupal.ajax({
            url: url,
            progress: {
              type: "fullscreen"
            }
          }).execute();
        });
      });
      $('.link-detail-popup a').once('click').each(function(){
        $(this).on('click', function(e){
          e.preventDefault();
          var url = $(this).attr('href');
          Drupal.ajax({
            url: url,
            dialog: {
              width: '90%'
            },
            dialogType: 'modal',
            progress: {
              type: "fullscreen"
            }
          }).execute();
        });
      });

      $('[data-toggle=popover]').once('popover').each(function(){
        //if($(this).is('a')){
          $(this).on('click', function(e){
            e.preventDefault();
          });
        //}
        $(this).popover();
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
