<?php
/**
 * @file
 * Contains Drupal\reforestation\TwigExtension.
 */

namespace Drupal\reforestation\TwigExtension;


/**
 * A Twig extension (filter) converts hex color to rgba.
 */
class ReforestationTwig extends \Twig_Extension {

  /**
   * Gets a unique identifier for this Twig extension.
   */
  public function getName() {
    return 'reforestation.twig.filters';
  }

  /**
   * convert 3 or 6 char hex color to comma seperated RGBA colors
   */
  public static function hexToRgba($hex=000000, $opacity=1) {
    $r = $g = $b = FALSE;
    $hex = str_replace("#", "", $hex);

    if(strlen($hex) == 3) {
      $r = hexdec(substr($hex, 0, 1).substr($hex, 0, 1));
      $g = hexdec(substr($hex, 1, 1).substr($hex, 1, 1));
      $b = hexdec(substr($hex, 2, 1).substr($hex, 2, 1));
    } else {
      $r = hexdec(substr($hex, 0, 2));
      $g = hexdec(substr($hex, 2, 2));
      $b = hexdec(substr($hex, 4, 2));
    }

    $rgba = array($r, $g, $b, $opacity);

    return implode(",", $rgba);
  }

	public static function ConvertFloatToInt($value) {
			$values = explode('.', $value);
			return $values[0];
	}

  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('hexToRgba', array($this, 'hexToRgba')),
      new \Twig_SimpleFunction('ConvertFloatToInt', array($this, 'ConvertFloatToInt')),
    ];
  }
}
