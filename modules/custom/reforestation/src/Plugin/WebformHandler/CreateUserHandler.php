<?php

namespace Drupal\reforestation\Plugin\WebformHandler;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\user\Entity\User;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "reforestation_create_user_handler",
 *   label = @Translation("Create User"),
 *   category = @Translation("Form Handler"),
 *   description = @Translation("Create new user from submission data"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class CreateUserHandler extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'field_maps' => 'email|email',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['field_maps'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Field maps'),
      '#description' => $this->t('Map webform field to user field. Example email|email'),
      '#default_value' => $this->configuration['field_maps'],
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValues();

    // Cleanup states.
    $values['states'] = array_values(array_filter($values['states']));

    foreach ($this->configuration as $name => $value) {
      if (isset($values[$name])) {
        // Convert options array to safe config array to prevent errors.
        // @see https://www.drupal.org/node/2297311
        if (preg_match('/_options$/', $name)) {
          $this->configuration[$name] = WebformOptionsHelper::encodeConfig($values[$name]);
        }
        else {
          $this->configuration[$name] = $values[$name];
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    // Your code here.
    // Get an array of the values from the submission.
    $new_user = User::create();

    $values = $webform_submission->getData();
    // Get the URL to post the data to.
    $fields = $this->configuration['field_maps'];
    foreach(preg_split("/((\r?\n)|(\r\n?))/", $fields) as $field){
      $maps = explode('|', $field);
      if (count($maps) == 2) {
        $wf_data = $this->getWebformData($maps[0], $webform_submission);
        if ($wf_data) {
          $new_user->set($maps[1], $wf_data);
        }
      }
    }
    //dsm($new_user);
    if($mail = $new_user->getEmail()){
      //Check if user existing already
      if(($old_user = user_load_by_mail($mail)) == FALSE){
        $username = static::uniqueUsername($mail);
        $new_user->setUsername($username);
        $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $new_user->set("langcode", $language);
        $password = user_password();
        $new_user->setPassword($password);
        $new_user->enforceIsNew();
        $new_user->activate();
        $new_user->addRole('project');
        $new_user->save();
        $webform_submission->setOwnerId($new_user->id());
        $webform_submission->save();
        _user_mail_notify('register_admin_created', $new_user);
      }else{
        $webform_submission->setOwnerId($old_user->id());
        $webform_submission->save();
      }
    }

    return true;
  }

  public static function uniqueUsername($name, $uid = 0) {
    $name = preg_replace('/@.*$/', '', $name);
    $i = 0;
    $database = \Drupal::database();
    // Iterate until we find a unique name.
    do {
      $new_name = empty($i) ? $name : $name . '_' . $i;
      $found = $database->queryRange("SELECT uid from {users_field_data} WHERE uid <> :uid AND name = :name", 0, 1, array(':uid' => $uid, ':name' => $new_name))->fetchAssoc();
      $i++;
    } while (!empty($found));

    return $new_name;
  }
  
  private function getWebformData($key, WebformSubmissionInterface $webform_submission){
    $keys = explode(':', $key);
    $data = $webform_submission->getData();
    foreach($keys as $k){
      $data = isset($data[$k]) ? $data[$k] : FALSE;
    }
    return $data;
  }

}
