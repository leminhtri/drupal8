<?php

namespace Drupal\reforestation\Plugin\WebformHandler;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\node\Entity\Node;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "reforestation_create_project_handler",
 *   label = @Translation("Create Project"),
 *   description = @Translation("Create new project from submission data"),
 *   category = @Translation("Form Handler"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class CreateProjectHandler extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'field_maps' => 'email|email',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['field_maps'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Field maps'),
      '#description' => $this->t('Map webform field to user field. Example email|email'),
      '#default_value' => $this->configuration['field_maps'],
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValues();

    // Cleanup states.
    $values['states'] = array_values(array_filter($values['states']));

    foreach ($this->configuration as $name => $value) {
      if (isset($values[$name])) {
        // Convert options array to safe config array to prevent errors.
        // @see https://www.drupal.org/node/2297311
        if (preg_match('/_options$/', $name)) {
          $this->configuration[$name] = WebformOptionsHelper::encodeConfig($values[$name]);
        }
        else {
          $this->configuration[$name] = $values[$name];
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    // Get an array of the values from the submission.
    $project = Node::create([
        'type' => 'project',
        'status' => 0,
    ]);
    
    $project->setOwnerId($webform_submission->getOwnerId());
    $values = $webform_submission->getData();
    $project_email = $values['e_mail'];
    //if($user = user_load_by_mail($project_email)){
    //  $project->setOwner($user);
    //}
    $fields = $this->configuration['field_maps'];
    foreach (preg_split("/((\r?\n)|(\r\n?))/", $fields) as $field) {
      $maps = explode('|', $field);
      if (count($maps) == 2) {
        $wf_data = $this->getWebformData($maps[0], $webform_submission);
        if ($wf_data) {
          $project->set($maps[1], $wf_data);
        }
      }else{
        switch ($maps[0]){
          case 'map_coordinates_project_location_latitude':
            
        }
      }
    }
    $project->save();

    return true;
  }
  
  private function getWebformData($key, WebformSubmissionInterface $webform_submission){
    $keys = explode(':', $key);
    $data = $webform_submission->getData();
    foreach($keys as $k){
      $data = isset($data[$k]) ? $data[$k] : FALSE;
    }
    return $data;
  }

}
