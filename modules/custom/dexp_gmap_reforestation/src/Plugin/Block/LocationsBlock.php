<?php

namespace Drupal\dexp_gmap_reforestation\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
/**
 * Provides a 'DrupalExp Gmap Locations ' Block
 *
 * @Block(
 *   id = "dexp_gmap_reforestation_locations",
 *   admin_label = @Translation("DrupalExp Locations block"),
 * )
 */
class LocationsBlock extends BlockBase {

	public function build() {
		$config = \Drupal::service('config.factory')->getEditable('dexp_gmap_reforestation.settings');
		$nids = \Drupal::entityQuery('node')->condition('type','project')->execute();
		$locations = [];
		foreach($nids as $nid){
			$node = Node::load($nid);
			$map_coordinates = $node->get('field_map_coordinates')->getValue();
            for($i = 0; $i < count($map_coordinates); $i++){
                $locations[] = [
                    'id' => $nid,
                    'link' => \Drupal\Core\Url::fromRoute('dexp_gmap_reforestation.project_detail', ['project_id' => $nid])->toString(),
                    'link_detail' => $node->url(),
                    'title' => $node->get('title')->value,
                    'latitude' => $map_coordinates[$i]['lat'],
                    'longitude' => $map_coordinates[$i]['lng'],
                ];
            }
        }
		return array(
		  '#markup' => '<div id="dexp-gmap-wrapper"></div><div id="project_detail"></div>',
		  '#attached' => array(
			'library' => array(
			  'dexp_gmap_reforestation/frontend',
			),
			'drupalSettings' => [
			  'dexp_gmap_locations' => $locations,
			  'dexp_gmap_settings' => array(
				'marker' => file_create_url($config->get('marker_icon_path')),
			  ),
			]
		  ),
		  '#cache' => ['max-age' => 0],
		);
	}
}
