<?php

namespace Drupal\dexp_gmap_reforestation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Class LocationSettingsForm.
 *
 * @package Drupal\dexp_gmap_reforestation\Form
 *
 * @ingroup dexp_gmap_reforestation
 */
class LocationSettingsForm extends ConfigFormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'Location_settings';
  }

  public function getEditableConfigNames() {
    return [
      'dexp_gmap_reforestation.settings',
    ];
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('dexp_gmap_reforestation.settings');
    $marker_icon = $form_state->getValue('marker_icon');
    if(!empty($marker_icon)) {
      $file = File::load($marker_icon[0]);
      if($file){
        $file->setPermanent();
        $file->save();
        $uri = $file->getFileUri();
        $form_state->setValue('marker_icon_path', $uri);
      }
    }
    foreach($form_state->getValues() as $key => $value){
      if(!in_array($key, array('form_build_id','submit','form_token', 'form_id', 'op'))){
        if(is_array($value) && isset($value['value'])){
          $value = $value['value'];
        }
        $config->set($key, $value);
      }
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }


  /**
   * Defines the settings form for Location entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = \Drupal::service('config.factory')->getEditable('dexp_gmap_reforestation.settings');
    $form['gmap_api_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Gmap API key'),
      '#default_value' => $config->get('gmap_api_key'),
    );
    $form['marker_icon_path'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Marker icon'),
      '#default_value' => $config->get('marker_icon_path'),
    );
    $form['marker_icon'] = array(
      '#type' => 'managed_file',
      '#upload_location' => 'public://',
      '#upload_validators'  => array(
        'file_validate_extensions' => array('gif png jpg jpeg svg'),
      ),
    );
    return $form;
  }

}
