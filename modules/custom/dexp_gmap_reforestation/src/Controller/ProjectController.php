<?php

namespace Drupal\dexp_gmap_reforestation\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\AjaxResponse;

class ProjectController extends ControllerBase {

  public function detail($project_id) {
    $entity_type = 'node';
    $view_mode = 'full';

    $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity_type);
    $storage = \Drupal::entityTypeManager()->getStorage($entity_type);
    $node = $storage->load($project_id);
    $build = $view_builder->view($node, $view_mode);
    $output = render($build);

    $response = new AjaxResponse();
    $response->addCommand(new \Drupal\Core\Ajax\HtmlCommand('#project_detail', $output));
    return $response;
  }

}
