(function ($, Drupal, drupalSettings) {
  var map = null;
  var infowindow = null;
  var userLocation = null;
  var userLocationMarker = null;
  Drupal.behaviors.dexp_gmap_admin = {
    attach: function (context, settings) {
      $('#dexp-gmap-wrapper').once('process').each(function () {
        var location = settings.dexp_gmap_location || [];
        var geocoder = new google.maps.Geocoder();
        // Init map
        var mapOptions = {
          scrollwheel: false,
          zoom: 14,
          mapTypeControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT
          }
        };
        map = new google.maps.Map(document.getElementById('dexp-gmap-wrapper'), mapOptions);
        infowindow = new google.maps.InfoWindow();
        // Init locations
        var markers = [];
        //$.each(locations, function (index, location) {
        var marker = createMarker(location);
        markers.push(marker);
        map.setCenter(new google.maps.LatLng(location.latitude, location.longitude));
        //});
        //Make sure all makers is showing in map
        //var bounds = new google.maps.LatLngBounds();
        //for (var i = 0; i < markers.length; i++) {
        //  bounds.extend(markers[i].getPosition());
        //}
        //map.fitBounds(bounds);
      });
    }
  };

  //Calculate distance
  function computeDistanceBetweenInMile(from, to) {
    var distance = google.maps.geometry.spherical.computeDistanceBetween(from, to);
    return Math.round(distance * 0.0621371192) / 100;
  }
  //Create marker
  function createMarker(location) {
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(location.latitude, location.longitude),
      map: map,
      icon: drupalSettings.dexp_gmap_settings.marker,
      animation: google.maps.Animation.DROP,
      title: location.title
    });
    marker.setMap(map);
    var $info = $('<div>').addClass('dexp-gmap-marker-info');
    $info.append('<h3 class="store-title">' + location.title + '</h3>');
    $info.append('<span class="store-distance"></span>');
    google.maps.event.addListener(marker, 'click', (function (marker) {
      return function () {
        if (userLocation !== null) {
          distance = computeDistanceBetweenInMile(new google.maps.LatLng(location.latitude, location.longitude), userLocation);
          $info.find('.store-distance').text(distance + ' miles away');
        }
        infowindow.setContent('<div class="infobox">' + $info.html() + '</div>');
        infowindow.open(map, marker);
      };
    })(marker));
    return marker;
  }
})(jQuery, Drupal, drupalSettings);
