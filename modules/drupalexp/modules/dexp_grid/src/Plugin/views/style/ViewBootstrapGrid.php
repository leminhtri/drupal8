<?php

/**
 * @file
 * Definition of Drupal\dexp_grid\Plugin\views\style\ViewBootstrapGrid.
 */
namespace Drupal\dexp_grid\Plugin\views\style;

use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Style plugin to render each item as a col in a Bootstrap row.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "views_dexp_grid_bootstrap",
 *   title = @Translation("DrupalExp: Bootstrap Grid"),
 *   help = @Translation("Displays rows in a Bootstrap column."),
 *   theme = "views_dexp_grid_bootstrap",
 *   display_types = {"normal"}
 * )
 */
class ViewBootstrapGrid extends StylePluginBase{
  
  protected $usesRowPlugin = TRUE;
  /**
   * Definition.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['wrapper_class'] = array('default' => '');
    $options['wrapper_row'] = array('default' => '');
    $options['wrapper_row_class'] = array('default' => '');
    $options['lg_cols'] = array('default' => 4);
    $options['md_cols'] = array('default' => 4);
    $options['sm_cols'] = array('default' => 2);
    $options['xs_cols'] = array('default' => 1);
    $options['col_padding'] = array('default' => '');
    return $options;
  }
  
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['wrapper_class'] = array(
        '#type' => 'textfield',
        '#title' => t('Wrapper class(es)'),
        '#description' => t('The class to provide on the wrapper, outside the grid.'),
        '#default_value' => $this->options['wrapper_class'],
    );
    $form['wrapper_row'] = array(
        '#type' => 'checkbox',
        '#title' => t('Add Row Wrapper'),
        '#default_value' => $this->options['wrapper_row'],
    );
    $form['wrapper_row_class'] = array(
        '#type' => 'textfield',
        '#title' => t('Row Wrapper Class'),
        '#default_value' => $this->options['wrapper_row_class'],
        '#states' => array(
          ':input[name=wrapper_row]' => array(
            'checked' => TRUE,
          ),
        ),
    );
    $form['lg_cols'] = array(
        '#type' => 'select',
        '#title' => t('LG columns'),
        '#description' => t('Number of columns on large screen (width ≥ 1200px)'),
        '#options' => array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 6 => 6, 12 => 12),
        '#default_value' => $this->options['lg_cols'],
    );
    $form['md_cols'] = array(
        '#type' => 'select',
        '#title' => t('MD columns'),
        '#description' => t('Number of columns on medium screen (width ≥ 992px)'),
        '#options' => array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 6 => 6, 12 => 12),
        '#default_value' => $this->options['md_cols'],
    );
    $form['sm_cols'] = array(
        '#type' => 'select',
        '#title' => t('SM columns'),
        '#description' => t('Number of columns on small screen (width ≥ 768px)'),
        '#options' => array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 6 => 6, 12 => 12),
        '#default_value' => $this->options['sm_cols'],
    );
    $form['xs_cols'] = array(
        '#type' => 'select',
        '#title' => t('XS columns'),
        '#description' => t('Number of columns on extra small screen (width < 768px)'),
        '#options' => array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 6 => 6, 12 => 12),
        '#default_value' => $this->options['xs_cols'],
    );
    $form['col_padding'] = array(
      '#type' => 'number',
      '#min' => 0,
      '#title' => t('Column padding'),
      '#field_suffix' => 'px',
      '#description' => t('Custom cloumn padding left and padding right. By default bootstrap value is 15px'),
      '#default_value' => $this->options['col_padding'],
    );
    
  }
}