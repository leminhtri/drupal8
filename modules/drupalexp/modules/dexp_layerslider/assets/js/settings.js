(function ($, Drupal) {
  "use strict";
  Drupal.behaviors.dexp_layerslider_settings = {
    attach: function () {
      $('form#dexp-layerslider-settings').once('dexp-submit').each(function () {
        $(this).submit(function () {
          var settings = {};
          $('.setting-option').each(function(){
            settings[$(this).attr('name')] = $(this).val();
          });
          $('input[name=settings]').val(JSON.stringify(settings));
        });
      });
    }
  };
})(jQuery, Drupal);