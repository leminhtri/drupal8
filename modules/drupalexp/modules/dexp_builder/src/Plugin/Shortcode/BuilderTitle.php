<?php

namespace Drupal\dexp_builder\Plugin\Shortcode;

use Drupal\Core\Language\Language;

/**
 * Provides a shortcode for title.
 *
 * @Shortcode(
 *   id = "dexp_builder_title",
 *   title = @Translation("Title"),
 *   description = @Translation("Render Title element like block title"),
 *   group = @Translation("Content"),
 *   child = {}
 * )
 */
class BuilderTitle extends BuilderElement {

  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {
    $attrs = $this->getAttributes(array(
      'title' => 'no',
      'subtitle' => '',
      'backword' => '',
      'class' => '',
        ), $attributes
    );
    $output = [
      '#theme' => 'dexp_builder_title',
      '#title' => $attrs['title'],
      '#subtitle' => $attrs['subtitle'],
      '#backword' => $attrs['backword'],
      '#class' => $attrs['class'],
    ];
    return $this->render($output);
  }
  
  public function processBuilder($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED){
    $attrs = $this->getAttributes(array(
      'title' => 'no',
      'subtitle' => '',
      'backword' => '',
      'class' => '',
        ), $attributes
    );
    return '<h2>' . $attrs['title'] . '</h2>';
  }

  public function settingsForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $form  = parent::settingsForm($form, $form_state);
    $form['general_options']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $this->get('title'),
    ];
    $form['general_options']['subtitle'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Sub title'),
      '#default_value' => $this->get('subtitle'),
    ];
    $form['general_options']['backword'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Backword'),
      '#default_value' => $this->get('backword'),
    ];
    $form['general_options']['class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom class'),
      '#default_value' => $this->get('class'),
    ];
    return $form;
  }
}