<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Drupal\dexp_builder\Plugin\Shortcode;

use Drupal\Core\Language\Language;

/**
 * @Shortcode(
 *   id = "dexp_builder_stats",
 *   title = @Translation("Stats"),
 *   description = @Translation("Render stats element"),
 *   group = @Translation("Content"),
 *   child = {}
 * )
 */
class BuilderStats extends BuilderElement{
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {
    $attrs = $this->getAttributes(array(
      'title' => 'no',
      'icon' => '',
      'number' => '',
      'duration' => 2000,
      'class' => '',
        ), $attributes
    );
    $output = [
      '#theme' => 'dexp_builder_stats',
      '#title' => $attrs['title'],
      '#class' => $attrs['class'],
      '#icon' => $attrs['icon'],
      '#number' => $attrs['number'],
      '#duration' => $attrs['duration'],
      '#attached' => ['library' => 'dexp_builder/stats'],
    ];
    
    return $this->render($output);
  }
  
  public function settingsForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form['general_options']['title'] = array(
      '#title' => $this->t('Title'),
      '#type' => 'textfield',
      '#default_value' => $this->get('title'),
    );
    
    $form['general_options']['icon'] = array(
      '#title' => $this->t('Icon'),
      '#type' => 'textfield',
      '#default_value' => $this->get('icon'),
      '#attributes' => ['class' => ['icon-select']],
    );
    
    $form['general_options']['number'] = array(
      '#title' => $this->t('Stats Number'),
      '#type' => 'number',
      '#default_value' => $this->get('number'),
    );
    
    $form['general_options']['duration'] = array(
      '#title' => $this->t('Duration'),
      '#type' => 'number',
      '#default_value' => $this->get('duration', 3000),
    );
    
    $form['general_options']['class'] = array(
      '#title' => $this->t('Custom class'),
      '#type' => 'textfield',
      '#default_value' => $this->get('class'),
    );
    
    return $form;
  }
}
