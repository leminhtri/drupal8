<?php

namespace Drupal\dexp_builder\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\Core\Template\Attribute;

/**
 * Provides a shortcode for empty space.
 *
 * @Shortcode(
 *   id = "dexp_builder_single_image",
 *   title = @Translation("Single Image"),
 *   description = @Translation("Add Single Image element"),
 *   group = @Translation("Content"),
 *   child = {}
 * )
 */
class BuilderSingleImage extends BuilderElement {
  
  public function process($attrs, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {
    $attrs = $this->getAttributes(array(
      'image' => 0,
      'class' => '',
      'link' => '',
      'link_target' => 'self',
      'link_class' => '',
    ),$attrs);
    $fid = str_replace('file:', '', $attrs['image']);
    $link = '';
    if($attrs['link']){
      if($attrs['link'] == '#'){
        $link = $attrs['link'];
      }else{
        try{
          $link = \Drupal\Core\Url::fromUserInput($attrs['link'])->toString();
        }catch (\Exception $e){
          $link = $attrs['link'];
        }
      }
    }
    if($file = \Drupal\file\Entity\File::load($fid)){
      if($link){
        if($attrs['link_target']){
          if($attrs['link_target'] == '_lightbox'){
            if(\Drupal\Component\Utility\UrlHelper::isExternal($link)){
              return '<a rel="colobox" href="' . $link . '" class="iframe dexp-builder-lightbox ' . $attrs['link_class'] . '"><img class="' . $attrs['class'] . '" src="' . file_create_url($file->getFileUri()) . '"/></a>';
            }else{
              return '<a rel="colobox" href="' . $link . '" class="dexp-builder-lightbox ' . $attrs['link_class'] . '"><img class="' . $attrs['class'] . '" src="' . file_create_url($file->getFileUri()) . '"/></a>';
            }
          }else{
            return '<a target="' . $attrs['link_target'] . '" href="' . $link . '" class="' . $attrs['link_class'] . '"><img class="' . $attrs['class'] . '" src="' . file_create_url($file->getFileUri()) . '"/></a>';
          }
        }
      }else{
        return '<img class="' . $attrs['class'] . '" src="' . file_create_url($file->getFileUri()) . '"/>';
      }
    }
    return '';
  }
  
  public function settingsForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form['general_options']['image'] = array(
      '#type' => 'image_browser',
      '#title' => $this->t('Image'),
      '#default_value' => $this->get('image', 0),
    );
    $form['general_options']['class'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Custom class'),
      '#default_value' => $this->get('class', ''),
      '#description' => $this->t('Some useful class: img-responsive, center-block, img-rounded, img-circle, img-thumbnail')
    );
    $form['general_options']['add_link'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Add link'),
      '#default_value' => $this->get('add_link', 0),
    );
    $form['general_options']['link'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Image link to'),
      '#default_value' => $this->get('link', ''),
      '#states' => array(
        'visible' => array(
          ':input[name=add_link]' => array('checked' => true),
        ),
      ),
    );
    $link_target_options = array(
      '_self' => $this->t('Same window'),
      '_blank' => $this->t('New window'),
      '_lightbox' => $this->t('Lightbox'),
    );
    if(\Drupal::service('module_handler')->moduleExists('dexp_page_settings')){
      $link_target_options['_lightbox'] = $this->t('ng_lightbox');
    }
    $form['general_options']['link_target'] = array(
      '#type' => 'select',
      '#title' => $this->t('Link targer'),
      '#options' => $link_target_options,
      '#default_value' => $this->get('link_target', ''),
      '#states' => array(
        'visible' => array(
          ':input[name=add_link]' => array('checked' => true),
        ),
      ),
    );
    $form['general_options']['link_class'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Link class'),
      '#default_value' => $this->get('link_class', ''),
      '#states' => array(
        'visible' => array(
          ':input[name=add_link]' => array('checked' => true),
        ),
      ),
    );
    return $form;
  }
}
