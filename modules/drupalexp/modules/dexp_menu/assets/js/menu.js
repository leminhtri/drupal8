(function($, Drupal){
  Drupal.behaviors.dexp_menu = {
    attach: function(context){
      $('ul.dexp-menu').once('dexp-menu').each(function(){
        var $menu = $(this);
        $menu.show();
        $menu.find('li.menu-item--expanded').hoverIntent({
          over: function(){
            $(this).addClass('hover');
            var $submenu  = $(this).find('>ul, >div');
            $submenu.css('transform','translateX(0)');
            if($(window).width() < 992) return;
            var $body_inner = $('div.dexp-body-inner');
            //if($submenu.hasClass('container')){
            if($submenu.is('[data-width=full-container],[data-width=full-screen]')){
              var transformX = $(window).width()/2 - $submenu.outerWidth()/2 - $submenu.offset().left;
              $submenu.css('transform','translateX('+transformX+'px)');
            }else{
              /*LTR*/
              var submenuwidth = $submenu.data('width')||'0';
              if(submenuwidth !== '0'){
                $submenu.css({width:submenuwidth});
              }
              var transformX = $body_inner.width() - $submenu.offset().left - $submenu.outerWidth();
              if($submenu.hasClass('depth-1')){
                if(transformX < 0){
                  $submenu.css('transform','translateX('+transformX+'px)');
                }
              }else{
                if(transformX < 0){
                  $submenu.addClass('opposite');
                }
              }
            }
          },
          out: function(){
            $(this).removeClass('hover');
          },
          timeout: 100,
          interval: 10
        });
      });
    }
  };
  
  Drupal.behaviors.dexp_menu_mobile = {
    attach: function(){
      $('span.dexp-menu-toogle').once('click').each(function(){
        $(this).click(function(e){
          e.preventDefault();
          var $menu = $('ul.menu.dexp-menu');
          if($('body').hasClass('menu-open') === false){
            $menu.css('transform','translateX(0)');
          }
          var transformX = 0 - $menu.offset().left;
          $menu.css('transform','translateX('+transformX+'px)');
          $('body').toggleClass('menu-open');
        });
      });
      
      $('span.dexp-submenu-toogle').once('click').each(function(){
        $(this).click(function(e){
          e.preventDefault();
          $(this).parent().toggleClass('menu-open');
        });
      });
      
      $(window).on('resize', function(){
        if($(window).width() > 991){
          $('ul.menu.dexp-menu').css('transform','translateX(0)');
        }
      });
    }
  };
})(jQuery, Drupal);